use yew::{function_component, html, Html, classes};

#[function_component]
pub fn Navigation() -> Html 
{
    html! 
    {
        <nav class={{classes!("side-nav")}}>
            <ul>
                <li><button><span class={{classes!("material-symbols-outlined")}}>{"space_dashboard"}</span>{"Dashboard"}</button></li>
                <li><button><span class={{classes!("material-symbols-outlined")}}>{"lists"}</span>{"Grocery Lists"}</button></li>
                <li><button><span class={{classes!("material-symbols-outlined")}}>{"grocery"}</span>{"Grocery Items"}</button></li>
                <li><button><span class={{classes!("material-symbols-outlined")}}>{"menu_book"}</span>{"recipes"}</button></li>
                <li><button><span class={{classes!("material-symbols-outlined")}}>{"nutrition"}</span>{"Nutrition facts"}</button></li>
            </ul>
            <hr />
            <ul> 
                <li><button><span class={{classes!("material-symbols-outlined")}}>{"settings"}</span>{"settings"}</button></li>
            </ul>
        </nav>
    }
}