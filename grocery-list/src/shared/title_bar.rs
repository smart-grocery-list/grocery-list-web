use yew::{function_component, html, Html, classes};

#[function_component]
pub fn TitleBar() -> Html
{
    html! 
    {
        <div class={classes!("title-bar")}>
            <button class={classes!("")}>
                <span class={classes!("material-symbols-outlined")}>{"menu"}</span>
            </button>
            <span>{"Grocery Guesser"}</span>
            <button>
                <span class={classes!("material-symbols-outlined")}>{"person"}</span>
            </button>
        </div>
    }
}