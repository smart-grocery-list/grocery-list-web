use yew::prelude::*;
use super::shared::title_bar::TitleBar;
use super::shared::navigation::Navigation;

#[function_component(App)]
pub fn app() -> Html {
    html! {
        <main>
            <TitleBar />
            <Navigation />
            <h1>{ "Welcome to Grocery Guesser" }</h1>
            <span class="subtitle">{ "Coming Soon " }</span>
        </main>
    }
}
