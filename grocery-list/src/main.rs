mod app;
mod shared;


use app::App;

fn main() {
    yew::Renderer::<App>::new().render();
}
